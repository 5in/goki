pragma solidity ^0.4.23;

import "./Goki.sol";
import "./crowdsale/RefundableCrowdsale.sol";
import "./crowdsale/CappedCrowdsale.sol";
import "./token/TokenTimelock.sol";


/**
* @dev This contract is the Goki token crowdsale and presale contract.
* @author Blockchain Partner
**/

contract GokiCrowdsale is RefundableCrowdsale, CappedCrowdsale {

  // whitelists
  mapping (address => bool) public whitelistCrowdsale;
  mapping (address => bool) public whitelistPresale;

  // date & time
  uint256 public startGeneralSale;

  // rates
  uint256 public constant generalPrice = 1 finney;
  uint256 public constant afterPresalePrice = 900 szabo;
  uint256 public constant beforePresalePrice = 800 szabo;
  uint256 public constant over1000EthPrice = 700 szabo;
  uint256 public constant over2000EthPrice = 600 szabo;

  // caps
  uint256 public constant presaleTarget = 40000 ether;
  uint256 public constant presaleParticipationMinimum = 1 ether;
  uint256 public constant crowdsaleParticipationMinimum = 10 finney;

  // lockups
  uint256 public constant managersLockup = 1 years;

  // wallets : these addresses will change
  address public constant managersWallet = 0x0032dd3EcfF7B672A8E2C28FCAb1Aa1cC50F81f1;
  address public constant receivingWallet = 0xE7305033fE4D5994Cd88d69740E9DB59F27c7046;

  // vesting contracts
  address public vestingManagers;

  // metrics
  uint256 public weiRaisedPresale;
  bool public hasReleased = false;
  bool public timeChanged = false;

  // participation
  mapping (address => uint) public presaleParticipation;
  mapping (address => uint) public crowdsaleParticipation;


  /**
  * @dev Constructor of the contract ; init startGeneralSale and crowdsale contracts
  * @param _startDate the start of the presale
  * @param _startGeneralSale start of the general sale
  * @param _endDate end of the crowdsale
  * @param _goal soft cap
  * @param _cap hard cap
  * @param _wallet address that will receive the funds
  **/
  constructor(uint256 _startDate, uint256 _startGeneralSale, uint256 _endDate,
                          uint256 _goal, uint256 _cap,
                          address _wallet)
      public
      CappedCrowdsale(_cap)
      FinalizableCrowdsale()
      RefundableCrowdsale(_goal)
      TimedCrowdsale(_startDate, _endDate)
      Crowdsale(generalPrice, _wallet, createTokenContract())
  {
      require(_goal <= _cap, "goal is superior to cap");
      require(_startGeneralSale > _startDate, "general sale is starting before presale");
      require(_endDate > _startGeneralSale, "sale ends before general start");

      startGeneralSale = _startGeneralSale;
  }

  /**
  * @dev Creates the Goki.
  * @return the Goki address
  **/
  function createTokenContract()
      internal
      returns (MintableToken)
  {
      return new Goki();
  }

  /**
  * @dev Checks if the sender is whitelisted for the presale.
  **/
  modifier onlyPresaleWhitelisted()
  {
      require(isWhitelistedPresale(msg.sender), "address is not whitelisted for presale");
      _;
  }

  /**
  * @dev Checks if the sender is whitelisted for the crowdsale.
  **/
  modifier onlyWhitelisted()
  {
      require(isWhitelisted(msg.sender) || isWhitelistedPresale(msg.sender),
              "address is not whitelisted for sale");
      _;
  }

  /**
  * @dev Check if the user is whitelisted for the crowdsale.
  * @return true if user is whitelisted ; false otherwise
  **/
  function isWhitelisted(address _user)
      public
      constant
      returns (bool)
  {
      return whitelistCrowdsale[_user];
  }

  /**
  * @dev Check if the user is whitelisted for the presale.
  * @return true if user is whitelisted ; false otherwise
  **/
  function isWhitelistedPresale(address _user)
      public
      constant
      returns (bool)
  {
      return whitelistPresale[_user];
  }

  function whitelistAddressPresale(address[] _usersPresale)
      public
      onlyOwner
  {
    for(uint i = 0 ; i < _usersPresale.length ; i++) {
      whitelistPresale[_usersPresale[i]] = true;
    }
  }

  function whitelistAddressCrowdsale(address[] _usersCrowdsale)
      public
      onlyOwner
  {
    for(uint j = 0 ; j < _usersCrowdsale.length ; j++) {
      whitelistCrowdsale[_usersCrowdsale[j]] = true;
    }
  }

  function unWhitelistCrowdsale(address[] _users)
      public
      onlyOwner
  {
    for(uint i = 0 ; i < _users.length ; i++) {
      whitelistCrowdsale[_users[i]] = false;
    }
  }

  function unWhitelistPresale(address[] _users)
      public
      onlyOwner
  {
    for(uint i = 0 ; i < _users.length ; i++) {
      whitelistPresale[_users[i]] = false;
    }
  }

  /**
  * @dev Fallback function redirecting to buying tokens functions depending on the time period.
  **/
  function ()
      external
      payable
  {
      if (now >= openingTime && now < startGeneralSale){
        buyTokensPresale(msg.sender);
      } else {
        buyTokens(msg.sender);
      }
  }

  /**
   * @dev Mints tokens corresponding to the transaction value for a whitelisted user during the crowdsale.
   * @param beneficiary the user wanting to buy tokens
   */
  function buyTokens(address beneficiary)
      public
      payable
      onlyWhitelisted
  {
      require(beneficiary != 0x0, "beneficiary cannot be 0x0");
      require(validPurchase(), "purchase is not valid");

      uint256 weiAmount = msg.value;
      uint256 tokens = weiAmount.mul(1 ether).div(generalPrice);
      weiRaised = weiRaised.add(weiAmount);

      token.mint(beneficiary, tokens);
      emit TokenPurchase(msg.sender, beneficiary, weiAmount, tokens);
      _forwardFunds();

      // check if we need to release half of the funds if soft cap has just been reached
      if(!hasReleased && weiRaised.add(weiRaisedPresale) >= goal) {
        vault.releaseHalf();
        hasReleased = true;
      }
  }

  /**
   * @dev Mints tokens corresponding to the transaction value for a whitelisted user during the presale.
   * @param beneficiary the user wanting to buy tokens
   */
  function buyTokensPresale(address beneficiary)
      public
      payable
      onlyPresaleWhitelisted
  {
      require(beneficiary != 0x0, "beneficiary cannot be 0x0");
      require(validPurchasePresale(), "presale purchase is not valid");

      // minting tokens at general rate because these tokens are not timelocked
      uint256 weiAmount = msg.value;
      uint256 tokens = getBonusTokens(msg.value);

      weiRaisedPresale = weiRaisedPresale.add(weiAmount);

      token.mint(beneficiary, tokens);
      emit TokenPurchase(msg.sender, beneficiary, weiAmount, tokens);
      _forwardFunds();

      // check if we need to release half of the funds if soft cap has just been reached
      if(!hasReleased && weiRaised.add(weiRaisedPresale) >= goal) {
        vault.releaseHalf();
        hasReleased = true;
      }

      // check if we change general date start since target has been reached
      if (weiRaisedPresale >= presaleTarget && !timeChanged) {
        if (now.add(30 days) < startGeneralSale) {
          startGeneralSale = now.add(30 days);
          closingTime = now.add(60 days);
          timeChanged = true;
        }
      }
  }

  /**
   * @dev Returns the number of tokens to be minted for a presale transaction
   * @param value presale transaction value
   */
  function getBonusTokens(uint value)
      internal
      view
      returns (uint)
  {
      uint toGenerate;
      if(value >= 2000 ether) {
        toGenerate = value.mul(1 ether).div(over2000EthPrice);
      } else if (value >= 1000 ether) {
        toGenerate = value.mul(1 ether).div(over1000EthPrice);
      } else if (weiRaisedPresale >= presaleTarget) {
        toGenerate = value.mul(1 ether).div(afterPresalePrice);
      } else {
        // check if it reaches target
        if(weiRaisedPresale.add(value) >= presaleTarget) {
          // (What has been raised (incl this contribution) - presale target)/ after target price
          toGenerate = (weiRaisedPresale.add(value).sub(presaleTarget)).mul(1 ether).div(afterPresalePrice);
          // (Presale target - what has been raised so far)/ before target price
          toGenerate = toGenerate.add((presaleTarget.sub(weiRaisedPresale)).mul(1 ether).div(beforePresalePrice));
        } else {
          toGenerate = value.mul(1 ether).div(beforePresalePrice);
        }
      }

      return toGenerate;
  }

  /**
  * @dev Checks if the crowdsale purchase is valid: correct time, value and hard cap not reached.
  * @return true if all criterias are satisfied ; false otherwise
  **/
  function validPurchase()
      internal
      returns (bool)
  {
      crowdsaleParticipation[msg.sender] = crowdsaleParticipation[msg.sender].add(msg.value);
      bool enough = crowdsaleParticipation[msg.sender] >= crowdsaleParticipationMinimum;
      bool withinPeriod = now >= startGeneralSale && now <= closingTime;
      bool nonZeroPurchase = msg.value != 0;
      uint256 totalWeiRaised = weiRaisedPresale.add(weiRaised);
      bool withinCap = totalWeiRaised.add(msg.value) <= cap;
      return withinCap && withinPeriod && nonZeroPurchase && enough;
  }

  /**
  * @dev Checks if the presale purchase is valid: correct time, value and hard cap not reached.
  * @return true if all criterias are satisfied ; false otherwise
  **/
  function validPurchasePresale()
      internal
      returns (bool)
  {
      presaleParticipation[msg.sender] = presaleParticipation[msg.sender].add(msg.value);
      bool enough = presaleParticipation[msg.sender] >= presaleParticipationMinimum;
      bool withinPeriod = now >= openingTime && now < startGeneralSale;
      bool nonZeroPurchase = msg.value != 0;
      bool withinCap = weiRaisedPresale.add(msg.value) <= cap;
      return withinPeriod && nonZeroPurchase && withinCap && enough;
  }

  /**
   * @dev Overriding the finalization method to add minting for managers/employees if soft cap is reached.
   */
  function finalization()
      internal
  {
      if (goalReached()) {
        // how many tokens have been sold
        uint256 currentSupply = token.totalSupply();
        // total supply calculated from tokens sold
        uint256 totalSupply = currentSupply.mul(100).div(60);

        // Managers tokens ; vesting not revocable ; 1 year cliff, vested for another year
        vestingManagers = new TokenTimelock(token, managersWallet, now.add(managersLockup));

        // advisors tokens : 5% ; manual vesting
        token.mint(receivingWallet, totalSupply.mul(5).div(100));
        // managers tokens : 15%
        token.mint(vestingManagers, totalSupply.mul(15).div(100));
        // employees tokens : ??%
        token.mint(receivingWallet, totalSupply.mul(10).div(100));
        // tokens reserve : ??%
        token.mint(receivingWallet, totalSupply.mul(10).div(100));

        token.finishMinting();
      }
      // if soft cap not reached ; vault opens for refunds
      super.finalization();
  }

  /**
  * @dev Override of the goalReached function in order to add presale weis to crowdsale weis and check if the total amount has reached the soft cap.
  * @return true if soft cap has been reached ; false otherwise
  **/
  function goalReached()
      public
      constant
      returns (bool)
  {
      uint256 totalWeiRaised = weiRaisedPresale.add(weiRaised);
      return totalWeiRaised >= goal || super.goalReached();
  }

  function hasClosed()
      public
      view
      returns (bool)
  {
    uint256 totalWeiRaised = weiRaisedPresale.add(weiRaised);
    return totalWeiRaised >= cap || super.hasClosed();
  }

}
