# Goin Crowdsale contracts

[![Build Status](https://travis-ci.org/Blockchainpartner/goin-crowdsale.svg?branch=master)](https://travis-ci.org/Blockchainpartner/goin-crowdsale)

Welcome to the repository containing the GoIn crowdsale smart contracts.

## Running the tests

### Prerequisites

```
npm install
```

### Try it yourself!

```
./scripts/test.sh
```

You can also launch your own testrpc instance and run

```
truffle test
```
